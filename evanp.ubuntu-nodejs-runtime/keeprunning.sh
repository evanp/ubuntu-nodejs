#!/bin/bash

pushd /opt/app

until npm start; do
    echo "Node process crashed with exit code $?.  Respawning.." >&2
    sleep 1
done

popd
